stocks = {
    "banana" : 666,
    "apple" : 333,
    "organge" : 111,
    "pear" : 2
}

prices = {
    "banana" : 4,
    "apple" : 2,
    "organge" : 1.5,
    "pear" : 3
}

store = [stocks, prices]

def printDict(dic):
    for entry in dic:
        print(entry, dic[entry])

def printStore(store):
    for i in store[0]:
        print(i)
        print("price: %d" % store[1][i])
        print("stock: %d" % store[0][i])
        
def potentialBenef(store):
    total = 0
    for i in store[0]:
        total += store[1][i] * store[0][i]
    print("Total potential benefice: %d" % total)

printDict(prices)
printStore(store)
potentialBenef(store)