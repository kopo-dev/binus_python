eren = {
    "name" : "Eren",
    "homework" : [90.0, 97.0, 75.0, 92.0],
    "quizzes" : [88.0, 40.0, 94.0],
    "tests" : [75.0, 90.0]
}

mikasa = {
    "name" : "Mikasa",
    "homework" : [100.0, 92.0, 98.0, 100.0],
    "quizzes" : [82.0, 83.0, 91.0],
    "tests" : [89.0, 97.0]
}

armin = {
    "name" : "Armin",
    "homework" : [0.0, 87.0, 75.0, 22.0],
    "quizzes" : [0.0, 75.0, 78.0],
    "tests" : [100.0, 100.0]
}

students = [eren, mikasa, armin]

def printStudentData(students):
    for i in students:
        for k, v in i.items():
            print(k, ":", v)

def average(numbers):
    total = float(sum(numbers))
    average = total/len(numbers)
    return (average)

def get_average(student):
    homework = average(student["homework"])
    quizzes = average(student["quizzes"])
    tests = average(student["tests"])
    homework = homework * (10 / 100)
    quizzes = quizzes * (30 / 100)
    tests = tests * (60 / 100)
    return (homework + quizzes + tests)

def get_letter_grade(score):
    if score > 90:
        return "A"
    elif score > 80:
        return "B"
    elif score > 70:
        return "C"
    elif score > 60:
        return "D"
    else:
        return "F"

def get_class_average(students):
    results = []
    for i in students:
        results.append(get_average(i))
    return average(results)

printStudentData(students)
print(average([5, 15, 20, 0]))
print(get_average(eren))
print(get_average(mikasa))
print(get_average(armin))
print(get_letter_grade(get_average(mikasa)))
print(get_class_average(students))