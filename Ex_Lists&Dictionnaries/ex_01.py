inventory = {
    'gold' : 500,
    'pouch' : ['flint', 'twine', 'gemstone'],
    'backpack' : ['xylophone', 'dagger', 'bedroll', 'bread loaf']
}
print(inventory)
print('Base inventory \n')

inventory['pocket'] = ['seashell', 'strange berry', 'lint']
print(inventory)
print('Added pocket\n')

inventory['backpack'].sort()
print(inventory)
print('Sorted backpack\n')

inventory['backpack'].remove('dagger')
inventory['gold'] += 50
print(inventory)
print('Traded dangerous weapon for some gold\n')

