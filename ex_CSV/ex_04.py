def get_csv_content(filename):
    fd = open('./sample_csv/' + filename, 'r')
    content = fd.read()
    return content

print(get_csv_content("activity.csv"))