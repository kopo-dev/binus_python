import matplotlib.pyplot as plt

def get_csv_content(filename):
    fd = open('./sample_csv/' + filename, 'r')
    content = fd.read()
    return content

def get_steps_per_day(data):
    steps = {}
    data = data.split("\n")
    data = data[1:-1]
    for line in data:
        line = line.split(',')
        if line[0] != "NA":
            line[1] = line[1][1:-1]
            steps[line[1]] = steps.get(line[1], 0) + int(line[0])
    return steps

def steps_per_day(steps):
    true_list = [vals for step, vals in steps.items() ]
    toast = plt.hist(true_list)
    print(true_list)
    plt.title = "Why ?"
    plt.show()

steps = get_steps_per_day(get_csv_content("activity.csv"))
steps_per_day(steps)