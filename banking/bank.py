import random


class Account:
    balance = 0

    def __init__(self, balance=0):
        self.deposit(balance)

    def getBalance(self):
        return self.balance

    def deposit(self, ammount):
        if ammount >= 0:
            self.balance += ammount
            print("New balance: %d" % (self.balance))
            return True
        else:
            print("Nice try, money doesn't work this way :(")
            return False

    def withdraw(self, ammount):
        if (ammount >= 0):
            self.balance -= ammount
            if (self.balance < 0):
                ammount += self.balance
                self.balance = 0
                print("You emptied your account\nYou withdrawed %d" % (ammount))
            else:
                print("You withdrawed %d\nNew balance: %d" %
                      (ammount, self.balance))
            return True
        else:
            print("Nice try, money doesn't work this way :(")
            return False


class Customer:
    firstName = ""
    lastName = ""
    account = None

    def __init__(self, first="Jonh", last="Doe"):
        self.firstName = first
        self.lastName = last
        self.account = Account(random.uniform(0, 999999999))
        print(self.firstName, self.lastName, self.account.getBalance())

    def getFirstName(self):
        return self.firstName

    def getLastName(self):
        return self.lastName

    def getAccount(self):
        return self.account

    def setAccount(self, acc):
        self.account = acc


class Bank:
    customers = []
    numberOfCustomers = 0
    bankName = ""

    def __init__(self, name="Fort Knox"):
        self.customers = []
        self.numberOfCustomers = 0
        self.bankName = name
        print(self.bankName, self.customers, self.numberOfCustomers)

    def addCustomer(self, first, last):
        self.customers.append(Customer(first, last))
        self.numberOfCustomers = len(self.customers)
        print("%s %s index is %d" %
              (first, last, (self.numberOfCustomers - 1)))

    def getNumOfCustomers(self):
        return self.numberOfCustomers

    def getCustomer(self, i):
        if (i < self.numberOfCustomers):
            return self.customers[i]
        print("Customer %d doesn't exist" % (i))

class Guichet():
    bank = None
    user = None
    command = []

    def __init__(self):
        self.newBank()
        self.displayHelp()
        self.initCustomer()
        self.setCommand()
        self.startWorking()

    def startWorking(self):
        while (self.command[0] != "exit"):
            self.displayCurrentCustomer()
            if self.command[0] == "add_customer":
                self.add_customer()
            elif self.command[0] == "change_customer":
                self.change_customer()
            elif self.command[0] == "number_customers":
                self.number_customers()
            elif self.command[0] == "deposit":
                self.deposit()
            elif self.command[0] == "withdraw":
                self.withdraw()
            else:
                print("Sorry Sir, I did not understand")
            self.setCommand()

    def displayHelp(self):
        print("Available commands are:")
        print("add_customer FIRSTNAME LASTNAME\n    Creates a new customer")
        print("change_customer CUSTOMER_INDEX\n    Change current customer")
        print("number_customers\n    Display total number of customers")
        print("deposit AMMOUNT\n    Deposit AMMOUNT money into current customer account")
        print("withdraw AMMOUNT\n    Withdraw AMMOUNT money from current customer account")
        print("exit\n    Kick out customers and close the bank")
    
    def initCustomer(self):
        name = input("Here is your first customer !\nWhat it's name ?\n-->")
        name = name.split(" ")
        self.bank.addCustomer(name[0], name[1])
        self.user = self.bank.getCustomer(0)

    def displayCurrentCustomer(self):
        print("Current customer: %s %s\nAccount balance: %d" % (
            self.user.getFirstName(), self.user.getLastName(), self.user.getAccount().getBalance()))
    
    def setCommand(self):
        command = input("How can I help you ?\n--> ")
        print("COMMANDE: \n", command.split(" "))
        self.command = command.split(" ")
        
    def newBank(self):
        name = input("What should be the name of your bank Sir ?\n--> ")
        self.bank = Bank(name)
    
    def add_customer(self):
        self.bank.addCustomer(self.command[1], self.command[2])
    
    def change_customer(self):
        try:
            index = int(self.command[1])
        except:
            print("That makes no sense sir !")
        if (index < self.bank.getNumOfCustomers()):
            self.user = self.bank.getCustomer(index)

    def number_customers(self):
        print("Our currently have %d customers" % (self.bank.getNumOfCustomers()))
    
    def deposit(self):
        try:
            ammount = int(self.command[1])
            self.user.getAccount().deposit(ammount)
        except:
            print("We do not accept Monopoly bills here !")
    
    def withdraw(self):
            ammount = int(self.command[1])
            self.user.getAccount().withdraw(ammount)
       

Guichet()