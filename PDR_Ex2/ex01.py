def getDays(hours, minutes, seconds):
    return ((hours + (minutes / 60) + (seconds / 360)) / 24)

def convertToDays():
    try:
        hours = int(input("hours ?\n--> "))
        minutes = int(input("minutes ?\n--> "))
        seconds = int(input("seconds ?\n--> "))
    except:
        print("Enter only numerical values")
        exit()
    print("total days: %f" % round(getDays(hours, minutes, seconds), 4))

convertToDays() 