def celToKelv(celTemp):
    return celTemp + 273.15

def farToCel(farTemp):
    return (5 / 9 * (farTemp - 32))

def temp():
    try:
        farTemp = float(input("Temperature in Fahrenheit ?\n--> "))
    except:
        print("Enter only numerical values")
        exit()
    celTemp = farToCel(farTemp)
    print("The temperature in Fahrenheit is %f" % farTemp)
    print("The temperature in Celsius is %f" % celTemp)
    print("The temperature in Kelvin is %f" % celToKelv(celTemp))

temp()