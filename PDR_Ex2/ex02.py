import math
def weightOnPlanet(weight, gravity = 23.1):
    planetWeight = (weight * gravity) / 9.8
    return planetWeight

try:
    weight = float(input("Weight on earth ?\n--> "))
    gravity = float(input("Planet's gravity ?\n--> "))
except:
    print("Enter only numerical values")
    exit()

print("weight on the planet: %f" % weightOnPlanet(weight, gravity))
print("Weight on jupiter: %f" % weightOnPlanet(weight))
