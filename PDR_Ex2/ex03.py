def numAtoms(grams, atomWeight = 196.96):
    avogadro = 6.022*10**22
    moles = grams /atomWeight
    nbAtoms = moles * avogadro
    return nbAtoms

print(numAtoms(10))
print(numAtoms(10, 12.001))
print(numAtoms(10, 1.008))

try:
    grams = float(input("How many grams of you element ?\n--> "))
    atomWeight = float(input("Atomic weight of your element ?\n--> "))
except:
    print("Enter only numerical values")
    exit()

print(numAtoms(grams, atomWeight))
