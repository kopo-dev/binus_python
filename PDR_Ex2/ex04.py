def calcNewHeight():
    try:
        sourceWidth = float(input("current width of your picture ?\n--> "))
        sourceHeight = float(input("current height of your picture ?\n--> "))
        maxWidth = float(input("desired new width ?\n--> "))
    except:
        print("Enter only numerical values")
        exit()
    ratio = maxWidth / sourceWidth
    newHeight = sourceHeight * ratio
    print("new resolution: %fx%f" % (maxWidth, newHeight))

calcNewHeight()

