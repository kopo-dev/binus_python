# Palindrome checking

str_tocheck = input("Enter your string: ")

def nbEven(list):
    nlist = [x for x in list if x % 2 == 0]
    return len(nlist)

def revStr(str):
    return str[::-1]

def ispalindrome(input_str):
    return (input_str == input_str[::-1])

print(str_tocheck)
print(revStr(str_tocheck))
print(nbEven([2,3,4,8]))
print (ispalindrome(str_tocheck))

# Task 1: Create a function to return a reversed string (e.g Input: asda; Output: adsa)
# Task 2: Create a function to print the total of even numbers given an input of a list
# (e.g Input [0,3,4,5,6,7,9] Output 10)