# Introduction to function

# Simple calculator
firstNum = input("Enter your first number: ")
secNum = input("Enter your second number: ")

def substraction(num1, num2):
    return int(num1) - int(num2)

def division(num1, num2):
    if (int(num2) == 0):
        return "IMPOSSIBLE ! ARE YOU TRYING TO BREAK THE TIME SPACE CONTINUUM ?!"
    return int(num1) / int(num2)

def multiplication(num1, num2):
    return int(num1) * int(num2)

def addition(num_1, num_2):
    return int(num_1) + int(num_2)

print (addition(firstNum,secNum))

# Task - create the remaining functions for substraction, multiplication and division