def revStr(str):
    length = len(str) -1
    reversed = ""
    while (length >= 0):
        reversed += str[length]
        length -= 1
    return reversed

print(revStr("Bonjour morray"))