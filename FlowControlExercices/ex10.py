def salary():
    try:
        hours = float(input("Hours worked\n--> "))
        rate = float(input("Per hour rate\n--> "))
    except:
        print("Friend, numerical value only")
        return (1)
    pay = 0
    hoursPayed = 0
    while (hoursPayed < hours):
        if (hoursPayed < 40):
            pay += rate
        else:
            pay += rate * 1.5
        hoursPayed += 1
    print("Hours: %f\nRate: %f\nPay:%f" % (hours, rate, pay))

salary()