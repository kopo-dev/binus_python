def revStr(str):
    length = len(str) -1
    reversed = ""
    while (length >= 0):
        reversed += str[length]
        length -= 1
    return reversed

def sentencePalindrome(string):
    newStr = (''.join(ch for ch in string if ch.isalnum()))
    newStr = newStr.lower()
    revdStr = revStr(newStr)
    if (newStr == revdStr.lower()):
        return True
    return False

print(sentencePalindrome("Salut morray !!!!!!"))
print(sentencePalindrome("Was it a rat I saw?"))