def scoring():
    try:
        score = float(input("Please input your score between 1.0 and 0.0\n--> "))
    except:
        print("Numerical value only my friend")
        return(1)
    if (score > 1.0 or score < 0.0):
        print("Score is out of range")
    elif (score >= 0.9):
        print("Grade A")
    elif (score >= 0.8):
        print("Grade B")
    elif (score >= 0.7):
        print("Grade C")
    elif (score >= 0.6):
        print("Grade D")
    else:
        print("Grade F")
    return 0

scoring()