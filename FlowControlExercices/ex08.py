def generate_n_chars(n, c):
    res = ""
    while (n > 0):
        res += c
        n -= 1
    return res

print(generate_n_chars(10,'W'))