def distance_from_zero(nb):
    if (type(nb) is int or type(nb) is float):
        if (nb < 0):
            nb *= -1
        return nb
    return "Nope"

print(distance_from_zero(-9.9))
print(distance_from_zero("wuuut?"))