def revStr(str):
    length = len(str) -1
    reversed = ""
    while (length >= 0):
        reversed += str[length]
        length -= 1
    return reversed

def isPalindrome(str):
    reversed = revStr(str)
    if (str == reversed):
        return True
    return False

print(isPalindrome("radar"))
print(isPalindrome("morray"))
