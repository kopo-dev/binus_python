'''
    move N E W S Distance 
    location
    reset
    exit
'''
def cmdReset():
    print("Reseting my position")
    return [0,0]

def cmdLocation(pos):
    print("My current location is (%d,%d)" % (pos[0],pos[1]))
    return 0

def cmdMove(cmd, pos):
    if (cmd[1] == 'n'):
        print("Taking %d pace North" % (cmd[2]))
        pos[1] += int(cmd[2])
    elif(cmd[1] == 'e'):
        print("Taking %d pace East" % (cmd[2]))
        pos[0] += int(cmd[2])
    elif(cmd[1] == 'w'):
        print("Taking %d pace West" % (cmd[2]))
        pos[0] -= int(cmd[2])
    elif(cmd[1] == 's'):
        print("Taking %d pace South" % (cmd[2]))
        pos[1] -= int(cmd[2])
    else:
        print("I ain't moving, you don't know what you're saying")
    return pos

def handleCommand(cmd, pos):
    res = pos
    if (cmd[0] == "move"):
        try:
            cmd[2] = int(cmd[2])
        except:
            print("Invalid argument for move")
        res = cmdMove(cmd, pos)
    elif(cmd[0] == "location"):
        cmdLocation(pos)
    elif(cmd[0] == "reset"):
        res = cmdReset()
    return res

def getCommand():
    cmd = input("What are the orders ?\n--> ")
    cmd = cmd.lower()
    if (len(cmd) < 0 or not cmd[0].isalpha()):
        print("I cannot do that sir")
        return False
    return cmd.split()
    

def mrRobot():
    pos = [0, 0]
    cmd = [""]
    while (cmd[0] != "exit"):
        cmd = getCommand()
        if (cmd):
            pos = handleCommand(cmd, pos)
            print("New position: %d,%d" % (pos[0], pos[1]))
    return 0

mrRobot()