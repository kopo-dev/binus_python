class Spell:
    def __init__(self, incantation, name):
        self.name = name
        self.incantation = incantation

    def __str__(self):
        return self.name + ' ' + self.incantation + '\n' + self.get_description()

    def get_description(self):
        return 'No description'

    def execute(self):
        print (self.incantation)

class Accio(Spell):
    def __init__(self):
        Spell.__init__(self, 'Accio', 'Summoning Charm')

    def __str__(self):
        return 'This charm summons an object to the caster, potentially over a significant distance'

class Confundo(Spell):
    def __init__(self):
        Spell.__init__(self, 'Confundo', 'Confundus Charm')
    
    def get_description(self):
        return 'Causes the victim to become confused and befuddled.'
    
def study_spell(spell):
    print(spell)

spell = Accio()
spell.execute()
study_spell(spell)
study_spell(Confundo())
print(Accio())

'''
a. What are the parent and child classes here?
    Parent class is Spell, Accio and Confundo are both child class to Spell

b. What does the code print out? (Try figuring it out without running it in Python)
    Accio
    Summoning Charm Accio
    No Description
    Confundus Charm Confundo
    Causes the victim to become confused and befuddled.

c. Which get description method is called when ‘study spell(Confundo())’ is executed? Why?
    The one from Confundo because if there is an instance self while refer at the instance first before the parent class

d. What do we need to do so that ‘print Accio()’ will print the appropriate description (‘This charm summons
an object to the caster, potentially over a significant distance’)?
Write down the code that we need to add and/or change.
    If we want to be able to do "print(Accio())" we would have to overload the str() method on the Accio class in order to print whatever we want
    As I did in the above code
'''
