class Staff():
    def __init__(self, id, name, position, salary):
        self.id = id
        self.name = name
        self.position = position
        self.salary = salary
    
    def __str__(self):
        return "|%s\t|%s\t\t|%s\t\t\t|%s\t|" % (self.id, self.name, self.position, self.salary)

    def getId(self):
        return self.id

    def getName(self):
        return self.name

    def getPosition(self):
        return self.position

    def getSalary(self):
        return self.salary

class Company():
    staffs = []
    def __init__(self):
        self.staffs = self.getStaffs('data.txt')
        self.displayRecords()
        while(True):
         self.handleMenu()

    def getStaffs(self, file):
        staffs = []
        fd = open(file, 'r')    
        content  = fd.read()
        content = content.split('\n')
        print(len(content))
        if(len(content) == 1):
            print("Empty data file :(")
        else:
            for staff in content:
                staff = staff.split('#')
                if (len(staff) == 4):
                    staffs.append(Staff(staff[0], staff[1], staff[2], staff[3]))
        fd.close()
        return staffs

    def displayRecords(self):
        dic = {}
        for staff in self.staffs:
            dic[staff.getName()] = staff
        order = sorted(dic)
        print("|ID\t|Name\t\t|Position\t\t\t|Salary\t\t|")
        for staff in order:
            print(dic[staff])
        print("\n")

    def getStaffById(self, id):
        dic = {}
        for staff in self.staffs:
            dic[staff.getId()] = staff
        return dic[id]

    def getIds(self):
        ids = []
        for staff in self.staffs:
            ids.append(staff.getId())
        return(ids)

    def newSalary(self):
        id = ""
        name = ""
        position = ""
        salary = ""
        while True:
            id = input("New ID: ")
            if (len(id) == 5 and id[0] == "S" and id[1:].isnumeric() and id not in self.getIds()):
                print("ID is valid")
                break
            else:
                print("Invalid ID, maybe already in use or wrong format")
        while True:
            name = input("New Name: ")
            if (len(name) <= 20):
                print("Valid name")
                break
            else:
                print("name shouldn't be longer than 20 characters")
        while True:
            print("Available position:\n1. Staff\n2. Officer\n3. Manager")
            choice = input("Position ?: ")
            if (choice == "1"):
                print("Valid position")
                position = "Staff"
                break
            elif(choice == "2"):
                print("Valid name")
                position = "Officer"
                break
            elif(choice == "3"):
                print("Valid name")
                position = "Manager"
                break
            else:
                print("Invalid input choice")
        while True:
            try:
                salary = int(input("Salary for %s: " % (position))) 
            except:
                print("Salary must be numbers, duh")
                continue
            if (position == "Staff" and salary >= 3500000 and salary <= 7000000):
                print("Valid salary")
                break
            elif (position == "Officer" and salary >= 7000001 and salary <= 10000000):
                print("Valid salary")
                break
            elif (position == "Manager" and salary > 10000000):
                print("Valid salary")
                break
            else:
                print("Invalid Salary")
        salary = str(salary)
        self.staffs.append((Staff(id, name, position, salary)))

    def deleteStaff(self):
        id = ""
        while True:
            id = input("Staff ID: ")
            if (len(id) == 5 and id[0] == "S" and id[1:].isnumeric() and id in self.getIds()):
                print("ID is valid")
                break
            else:
                print("Invalid ID")
        print("Staff to delete:")
        print(self.getStaffById(id))
        self.staffs.remove(self.getStaffById(id))

    def displayData(self, position):
        mini = 0
        first = 1
        maxi = 0
        current = 0
        total = 0
        number = 0
        for staff in self.staffs:
            if (staff.getPosition() == position):
                if(first == 1):
                    mini = int(staff.getSalary())
                    first = 0
                current = int(staff.getSalary())
                if (current < mini):
                    mini = current
                if (current > maxi):
                    maxi = current
                total += current
                number += 1
        print("Minimum Salary: %d" % (mini))
        print("Maximum Salary: %d" % (maxi))
        print("Average Salary: %d" % (total / number))

    def showData(self):
        print("1. Staff")
        self.displayData("Staff")
        print("2. Officer")
        self.displayData("Officer")
        print("3. Manager")
        self.displayData("Manager")
    
    def save(self):
        toWrite = ""
        open("data.txt", "w").close()
        fd = open("data.txt", "w")
        for staff in self.staffs:
            toWrite = "%s#%s#%s#%s\n" % (staff.getId(), staff.getName(), staff.getPosition(), staff.getSalary() )
            fd.write(toWrite)
        print("Change saved")
        fd.close()


    def exit(self):
        print("Thank you and goodbye :)")
        exit()

    def handleMenu(self):
        print("1. New Staff\n2. Delete Staff\n3. View Summary Data\n4. Save & Exit")
        command = input("Input Choice: ")
        if (command == "1"):
            self.newSalary()
        elif(command == "2"):
            self.deleteStaff()
        elif(command == "3"):
            self.showData()
        elif(command == "4"):
            self.save()
            self.exit()
        else:
            command = ""
            print("Invalid choice")

Company()