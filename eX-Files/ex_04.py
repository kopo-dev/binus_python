def get_pokemons():
    fd = open("pokemon.txt", 'r')
    content = fd.read()
    fd.close()
    return content.split()

def get_sequence(start_index, pokemons):
    sequence = []
    current = pokemons[start_index]
    sequence.append(current)
    n = 0
    while n < len(pokemons):
        pokemon = pokemons[n]
        if pokemon not in sequence and pokemon[0] == current[-1]:
            current = pokemon
            sequence.append(current)
            n = 0
        n += 1
    return sequence

def get_longest_sequence():
    pokemons = get_pokemons()
    sequences = []
    n = 0
    while n < len(pokemons):
        sequences.append(get_sequence(n, pokemons))
        n += 1
    #print(*sequences, sep='\n') #I leave that line in because I love it
    return max(sequences, key=len)

print(get_longest_sequence())