def get_text(text):
    fd = open(text, 'r')
    content = fd.read()
    fd.close()
    return content

def sentence_splitter(file):
    text = get_text(file)
    new_text = ""
    n = 0
    while n < len(text) - 1:
        if text[n] == ".":
            if text[n + 1] == "." and text[n + 2] == ".":
                new_text += text[n]
                new_text += text[n + 1]
                n += 1
            elif text[n - 2].isupper() or text[n - 3].isupper():
                new_text += text[n] 
            elif text[n + 1] == " " and text[n + 2].islower():
                new_text += text[n]
            elif text[n + 1].isdigit():
                new_text += text[n]
            elif text[n + 1].isalpha():
                new_text += text[n]
            else: 
                new_text += text[n] + '\n'
                if text[n + 1] == " ":
                    n += 1
        elif text[n] == "?" or text[n] == "!":
            new_text += text[n] + '\n'
            if text[n + 1] == " ":
                n += 1
        else:
            new_text += text[n]
        
        n += 1
    new_text += text[-1]
    return new_text

print(sentence_splitter("text.txt"))
            
    