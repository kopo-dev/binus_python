def get_text(name):
    fd = open(name, 'r')
    content = fd.read()
    fd.close()
    return content.split('\n')

def number_text(name):
    lines = get_text(name)
    n = 1
    new_file = open("numerbed_" + name, 'w')
    for line in lines:
        new_file.write(str(n) + " " + line + "\n")
        n += 1
    new_file.close()

number_text("book.txt")