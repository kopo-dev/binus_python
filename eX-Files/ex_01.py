def get_text(name):
    fd = open(name, 'r')
    content = fd.read()
    fd.close()
    return content.lower()

def get_words_frequencies(text):
    frequencies = {}
    for word in text.split():
        frequencies[word] = frequencies.get(word, 0) + 1
    return frequencies

def get_hapax(frequencies):
    hapax = []
    for word, freq in frequencies.items():
        if freq  == 1:
            print(word, freq)
            hapax.append(word)   
    return hapax

def hapax_finder(name):
    text = get_text(name)
    frequencies = get_words_frequencies(text)
    return  get_hapax(frequencies)

print(hapax_finder("book.txt"))