def get_text(name):
    fd = open(name, 'r')
    content = fd.read()
    fd.close()
    return content.split()

def get_average_len(name):
    nb_word = 0
    total_len = 0
    text = get_text(name)
    for word in text:
        nb_word += 1
        total_len += len(word)
    return total_len / nb_word

print(get_average_len("book.txt"))