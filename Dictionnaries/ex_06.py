# Point was to use a dictionnary

def caesarDecode(encrypted, key):
    string = ""
    for char in encrypted:
        if(char.isalpha()):
            if (char.isupper()):
                code = ord(char) - 65
                code = ((code - key) % (ord('Z') - 64) + 65)
            else:
                code = ord(char) - 97
                code = ((code - key) % (ord('z') - 96) + 97)
            string += chr(code)
        else:
            string += char
    return string
        

def caesarEncode(string, key):
    encrypted = ""
    for char in string:
        if (char.isalpha()):
            if (char.isupper()):
                code = ord(char) - 65
                code = ((code + key) % (ord('Z') - 64) + 65)
            else:
                code = ord(char) - 97
                code = ((code + key) % (ord('z') - 96) + 97)
            encrypted += chr(code)
        else:
            encrypted += char
    return encrypted

text = input("Type the sentence you want to encrypt\n--> ")
rotation = int(input("Type the rotation to use for encryption\n--> "))

print("Encrypted version")
encrypted = caesarEncode(text, rotation)
print(encrypted)
print("Decrypted version")
print(caesarDecode(encrypted, rotation))