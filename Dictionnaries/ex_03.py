mydict = {"kopo": "kopass", "game": "dusk", "fun": "dusk"}

def findValue(mydict, value):
    results = []
    for key, val in mydict.items():
        if val == value:
            results.append(key)
    return results

print(findValue(mydict, "dusk"))