dna = "GTAGAGCTGTGTAGAGCTGTGTAGAGCTGTGTAGAGCTGTGTAGAGCTGT"

def wordFrequencies(myList):
    frequencies = {}
    for i in myList:
        frequencies[i] = frequencies.get(i, 0) + 1
    return frequencies

def kmer(dna, k):
    parts = [dna [i:i+k] for i in range(0, len(dna), k)]
    return wordFrequencies(parts)

print(kmer(dna, 5))
print(kmer(dna, 4))
print(kmer(dna, 3))
print(kmer(dna, 2))
print(kmer(dna, 1))