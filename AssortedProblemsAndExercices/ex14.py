def makeForming(verb):
    consonants = "zrtpqsdfghjklmwxcvbn"
    vowels = "aeiouy"
    if (verb.endswith('ie')):
        verb = verb[:-2] + 'ying'
    elif(verb.endswith('e')):
        verb = verb[:-1] + 'ing'
    elif (verb[-3] in consonants and verb[-2] in vowels and verb[-1] in consonants):
        verb += verb[-1] + "ing"
    else:
        verb += "ing"
    return verb

print(makeForming("hungry"))
print(makeForming("brush"))
print(makeForming("fix"))
print(makeForming("see"))
print(makeForming("hug"))
print(makeForming("lie"))
print(makeForming("sleep"))

