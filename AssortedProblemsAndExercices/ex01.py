def stringToList(string):
    string = string.split(',')
    return(string)

def listToTuple(list):
    tup = tuple(x for x in list)
    return(tup)

def stringConvertion(string):
    list = stringToList(string)
    tuple = listToTuple(list)
    print(list, tuple)
    return (list, tuple)

stringConvertion("1,2,3,4,5,6,7")