import calendar

def printMonth(year, month):
    print(calendar.month(year, month, w=0, l=0))

printMonth(2019, 10)