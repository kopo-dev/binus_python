def isOverlapping(l1, l2):
    for x in l1:
        for y in l2:
            if (x == y):
                return True
    return False

print(isOverlapping([1,2,3], [3,4,5]))
print(isOverlapping([1,2,3], [4,5,6]))
