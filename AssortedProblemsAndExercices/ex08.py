def longestWord(liste):
    max = 0
    maxWord = ''
    for word in liste:
        if (len(word) > max):
            max = len(word)
            maxWord = word
    return maxWord

print(longestWord(['Bonjour', "les", "copains", "je", "veux", "manger", "du", "saucisson", "et", "magres de canard"]))