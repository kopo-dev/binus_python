import string 

def isPangram(strin):
    strin.lower()
    alphabet = list(string.ascii_lowercase)
    for i in strin:
        if i in alphabet:
            alphabet.remove(i)
    return len(alphabet) == 0

print(isPangram("The quick brown fox jumps over the lazy dog"))
print(isPangram("The quic brown fox jumps over the lazy dog"))
