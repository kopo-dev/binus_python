def charFreq(string):
    freq = {}
    for char in string:
        if (char not in freq):
            freq[char] = 1
        else:
            freq[char] += 1
    return freq

print(charFreq("coucou les copains hihi"))