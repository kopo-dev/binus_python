def caesarDecode(encrypted, key):
    string = ""
    encrypted = encrypted.lower()
    for char in encrypted:
        if(char.isalpha()):
            code = ord(char) - 97
            code = ((code - key) % (ord('z') - 96) + 97)
            string += chr(code)
        else:
            string += char
    return string
        

def caesarEncode(string, key):
    encrypted = ""
    string = string.lower()
    for char in string:
        if (char.isalpha()):
            code = ord(char) - 97
            code = ((code + key) % (ord('z') - 96) + 97)
            encrypted += chr(code)
        else:
            encrypted += char
    return encrypted

print(caesarEncode("gros chevaucheur de poney xD", 13))
print(caesarDecode(caesarEncode("gros chevaucheur de poney xD", 13), 13))