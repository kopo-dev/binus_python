try:
    student1 = int(input("Class 1 number of student\n-> "))
    nbGroup1 = int(input("Class 1 number of group wanted\n-> "))
    student2 = int(input("Class 2 number of student\n-> "))
    nbGroup2 = int(input("Class 2 number of group wanted\n-> "))
    student3 = int(input("Class 3 number of student\n-> "))
    nbGroup3 = int(input("Class 3 number of group wanted\n-> "))
except:
    print("*angry beeping*\nENTER NON NUMERICAL VALUE ONE MORE TIME, I DARE YOU\n")
    exit()

groupSize1 = student1 / nbGroup1
leftOvers1 = student1 % nbGroup1
print("Class 1\nNumber of students: %d\nNumber of groups: %d\nStudents per group: %d\nLeftover students: %d" % (student1, nbGroup1, groupSize1, leftOvers1))
groupSize2 = student2 / nbGroup2
leftOvers2 = student2 % nbGroup2
print("Class 2\nNumber of students: %d\nNumber of groups: %d\nStudents per group: %d\nLeftover students: %d" % (student2, nbGroup2, groupSize2, leftOvers2))
groupSize3 = student3 / nbGroup3
leftOvers3 = student3 % nbGroup3
print("Class 3\nNumber of students: %d\nNumber of groups: %d\nStudents per group: %d\nLeftover students: %d" % (student3, nbGroup3, groupSize3, leftOvers3))
